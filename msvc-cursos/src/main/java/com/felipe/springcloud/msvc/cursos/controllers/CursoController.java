package com.felipe.springcloud.msvc.cursos.controllers;

import com.felipe.springcloud.msvc.cursos.models.Usuario;
import com.felipe.springcloud.msvc.cursos.models.entity.Curso;
import com.felipe.springcloud.msvc.cursos.services.CursoService;
import feign.FeignException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CursoController {

    @Autowired
    private CursoService service;

    @GetMapping("/")
    public ResponseEntity<List<Curso>> listar() {
        return ResponseEntity.ok(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> detalle(
        @PathVariable Long id
    ) {
        Optional<Curso> o = service.porIdConUsuarios(id); //service.porId(id);
        if (o.isPresent()) {
            return ResponseEntity.ok(o.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<?> crear(
        @Valid @RequestBody Curso curso,
        BindingResult result
    ) {
        if (result.hasErrors()) return validar(result);

        Curso cursoDB = service.guardar(curso);
        return ResponseEntity.status(HttpStatus.CREATED).body(cursoDB);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(
        @Valid @RequestBody Curso curso,
        BindingResult result,
        @PathVariable Long id
    ) {
        if (result.hasErrors()) return validar(result);

        Optional<Curso> optionalCurso = service.porId(id);
        if(optionalCurso.isPresent()) {
            Curso cursoDB = optionalCurso.get();
            cursoDB.setNombre(curso.getNombre());
            return ResponseEntity.status(HttpStatus.CREATED).body(service.guardar(cursoDB));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(
        @PathVariable Long id
    ) {
        Optional<Curso> optionalCurso = service.porId(id);
        if (optionalCurso.isPresent()) {
            service.eliminar(optionalCurso.get().getId());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/asignar-usuario/{cursoId}")
    public ResponseEntity<?> asignarUsuario(@RequestBody Usuario usuario, @PathVariable Long cursoId) {
        Optional <Usuario> usuarioAsignado;
        try {
            usuarioAsignado = service.asignarUsuario(usuario, cursoId);
        } catch (FeignException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Collections
                    .singletonMap("mensaje", "No existe el usuario por el id o error en la comunicación " + e.getMessage()));
        }
        if (usuarioAsignado.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(usuarioAsignado.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/crear-usuario/{cursoId}")
    public ResponseEntity<?> crearUsuario(@RequestBody Usuario usuario, @PathVariable Long cursoId) {
        Optional <Usuario> usuarioCreado;
        try {
            usuarioCreado = service.crearUsuario(usuario, cursoId);
        } catch (FeignException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Collections
                    .singletonMap("mensaje", "No se pudo crear el usuario o error en la comunicación " + e.getMessage()));
        }
        if (usuarioCreado.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(usuarioCreado.get());
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/eliminar-usuario/{cursoId}")
    public ResponseEntity<?> eliminarUsuario(@RequestBody Usuario usuario, @PathVariable Long cursoId) {
        Optional <Usuario> usuarioEliminado;
        try {
            usuarioEliminado = service.eliminarUsuario(usuario, cursoId);
        } catch (FeignException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Collections
                    .singletonMap("mensaje", "No existe el usuario por el id o error en la comunicación " + e.getMessage()));
        }
        if (usuarioEliminado.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(usuarioEliminado.get());
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/eliminar-curso-usuario/{id}")
    public ResponseEntity<?> eliminarCursoUsuarioPorId(@PathVariable Long id) {
        service.eliminarCursoUsuarioPoId(id);
        return ResponseEntity.noContent().build();
    }

    private ResponseEntity<Map<String, String>> validar(BindingResult result) {
        Map<String, String> errores = new HashMap<>();
        result.getFieldErrors().forEach(error -> {
            errores.put(error.getField(), "El campo " + error.getField() + " " + error.getDefaultMessage());
        });
        return ResponseEntity.badRequest().body(errores);
    }
}
