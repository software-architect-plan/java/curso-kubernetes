package com.felipe.springcloud.msvc.cursos.services;

import com.felipe.springcloud.msvc.cursos.clients.UsuarioClientRest;
import com.felipe.springcloud.msvc.cursos.models.Usuario;
import com.felipe.springcloud.msvc.cursos.models.entity.Curso;
import com.felipe.springcloud.msvc.cursos.models.entity.CursoUsuario;
import com.felipe.springcloud.msvc.cursos.repositories.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CursoServiceImpl implements CursoService{

    @Autowired
    CursoRepository repository;

    @Autowired
    private UsuarioClientRest usuarioClient;
    @Override
    @Transactional(readOnly = true)
    public List<Curso> listar() {
        return (List<Curso>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Curso> porId(Long id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Curso> porIdConUsuarios(Long id) {
        Optional<Curso> o = repository.findById(id);
        if (o.isPresent()) {
            Curso curso = o.get();
            if (!curso.getCursoUsuarios().isEmpty()) {
                // option 1 -> cu -> cu.getUsuarioId()
                // List<Long> ids = curso.getCursoUsuarios().stream().map(cu -> cu.getUsuarioId())
                //        .collect(Collectors.toList());
                // option 2 -> CursoUsuario::getUsuarioId
                List<Long> ids = curso.getCursoUsuarios().stream().map(CursoUsuario::getUsuarioId)
                        .collect(Collectors.toList());
                List<Usuario> usuarios = usuarioClient.obtenerAlumnosPorCurso(ids);
                curso.setUsuarios(usuarios);
            }
            return Optional.of(curso);
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public Curso guardar(Curso curso) {
        return repository.save(curso);
    }

    @Override
    @Transactional
    public void eliminar(Long id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void eliminarCursoUsuarioPoId(Long id) {
        repository.eliminarCursoUsuarioPorId(id);
    }

    @Override
    @Transactional
    public Optional<Usuario> asignarUsuario(Usuario usuario, Long cursoId) {
        Optional<Curso> cursoQuery = repository.findById(cursoId);
        if (cursoQuery.isPresent()) {
            Usuario usuarioMsvc = usuarioClient.detalle(usuario.getId());

            Curso curso = cursoQuery.get();
            CursoUsuario cursoUsuario =  new CursoUsuario();
            cursoUsuario.setUsuarioId(usuarioMsvc.getId());

            curso.addCursoUsuario(cursoUsuario);
            repository.save(curso);

            return Optional.of(usuarioMsvc);
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public Optional<Usuario> crearUsuario(Usuario usuario, Long cursoId) {
        Optional<Curso> cursoQuery = repository.findById(cursoId);
        if (cursoQuery.isPresent()) {
            Usuario usuarioNuevoMsvc = usuarioClient.crear(usuario);

            Curso curso = cursoQuery.get();
            CursoUsuario cursoUsuario =  new CursoUsuario();
            cursoUsuario.setUsuarioId(usuarioNuevoMsvc.getId());

            curso.addCursoUsuario(cursoUsuario);
            repository.save(curso);

            return Optional.of(usuarioNuevoMsvc);
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public Optional<Usuario> eliminarUsuario(Usuario usuario, Long cursoId) {
        Optional<Curso> cursoQuery = repository.findById(cursoId);
        if (cursoQuery.isPresent()) {
            Usuario usuarioMsvc = usuarioClient.detalle(usuario.getId());

            Curso curso = cursoQuery.get();
            CursoUsuario cursoUsuario =  new CursoUsuario();
            cursoUsuario.setUsuarioId(usuarioMsvc.getId());

            curso.removeCursoUsuario(cursoUsuario);
            repository.save(curso);

            return Optional.of(usuarioMsvc);
        }
        return Optional.empty();
    }
}
